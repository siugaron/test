<?php
namespace App\DB;

use App\Debug;

class DB extends DBBase
{

	/**
	 * @param $table
	 * @param string $fields
	 * @param bool $criteria
	 * @param array $holders
	 * @param bool $order
	 * @param bool $limit
	 *
	 * @return mixed
	 */
    public static function select($table, $fields = "*", $criteria = false, $holders = [], $order = false, $limit = false)
    {

        $query = [];
        $query[] = "SELECT {$fields} FROM {$table}";

        if ($criteria)
            $query[] = "WHERE {$criteria}";

        if ($order)
            $query[] = "ORDER BY {$order}";

        if ($limit)
            $query[] = "LIMIT {$limit}";

        $stmt = self::execute(implode(" ", $query), $holders);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }

    /**
     * @param $table
     * @param array $list
     * @param bool $criteria
     * @param array $holders
     * @param bool $order
     * @param bool $limit
     *
     * @return array
     */
    public static function selectList($table, $list = [], $criteria = false, $holders = [], $order = false, $limit = false)
    {
        if (count($list) != 2)
            exit("Wrong format for 2 argument");

        $raw = self::select($table, implode(",", $list), $criteria, $holders, $order, $limit);
        if (empty($raw))
            return [];

        $result = [];
        foreach ($raw as $r) {
            $result[$r[$list[0]]] = $r[$list[1]];
        }

        return $result;
    }


    /**
     * @param $query
     * @param $arguments
     *
     * @return mixed
     */
    public static function rows($query, $arguments = [])
    {
        if (!is_array($arguments)) {
            $arguments = func_get_args();
            array_shift($arguments);
        }

        $stmt = self::execute($query, $arguments);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }

    /**
     * @param $table
     * @param string $where
     * @param array $whereHolders
     *
     * @return int
     */
    public static function count($table, $where = "", $whereHolders = [])
    {
        $query = [];
        $query[] = "SELECT COUNT(*) FROM {$table}";

        if ($where)
            $query[] = "WHERE {$where}";


        $stmt = self::execute(implode(" ", $query), $whereHolders);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        return ($data) ? (int)current($data) : 0;
    }


    /**
     * @param $query
     * @param array $arguments
     *
     * @return mixed
     */
    public static function cell($query, $arguments = [])
    {
        if (!is_array($arguments)) {
            $arguments = func_get_args();
            array_shift($arguments);
        }

        $stmt = self::execute($query, $arguments);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        return ($data) ? current($data) : false;
    }

	/**
	 * @param $query
	 * @param array $arguments
	 *
	 * @return mixed
	 */
    public static function row($query, $arguments = [])
    {
        if (!is_array($arguments)) {
            $arguments = func_get_args();
            array_shift($arguments);
        }

        $stmt = self::execute($query, $arguments);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $data;
    }

    /**
     * @param $table
     * @param array $values
     * @param string $where
     * @param array $whereHolders
     *
     * @return bool
     */
    public static function update($table, $values = [], $where = "", $whereHolders = [])
    {
        $query = [];
        $query[] = "UPDATE {$table}";

        if (empty($values))
            return false;

        $holders = [];
        $holdersStr = "SET ";
        foreach ($values as $key => $val) {
            $holdersStr .= self::SQL_QUOTE . $key . self::SQL_QUOTE . "=?,";
            $holders[] = $val;
        }

        $query[] = trim($holdersStr, ",");

        if ($where)
            $query[] = "WHERE {$where}";

        $holders = array_merge($holders, $whereHolders);

        self::execute(implode(" ", $query), $holders);

        return (empty(self::$errors)) ? true : false;
    }

    /**
     * @param $table
     * @param array $values
     * @param string $where
     * @param array $whereHolders
     *
     * @return bool
     */
    public static function upsert($table, $values = [], $where = "", $whereHolders = [])
    {
        if (self::count($table, $where, $whereHolders) == 0) {
            return self::insert($table, $values);
        } else {
            return self::update($table, $values, $where = "", $whereHolders);
        }
    }

    /**
     * @param $table
     * @param array $values
     *
     * @return bool
     */
    public static function insert($table, $values = [])
    {
        $query = [];
        $query[] = "INSERT INTO {$table}";

        if (empty($values))
            return false;

        $holders = [];
        $fieldsStr = "";
        $holdersStr = "VALUES (";
        foreach ($values as $key => $val) {
            $holdersStr .= "?,";
            $fieldsStr .= $key . ",";
            $holders[] = $val;
        }

        $query[] = "(" . trim($fieldsStr, ",") . ")";
        $query[] = "" . trim($holdersStr, ",") . ")";
 
        self::execute(implode(" ", $query), $holders);
        return (empty(self::$errors)) ? true : false;
    }

    /**
     * @param $table
     * @param string $where
     * @param array $whereHolders
     *
     * @return bool
     */
    public static function delete($table, $where = "", $whereHolders = [])
    {
        $query = [];
        $query[] = "DELETE FROM {$table}";

        if ($where)
            $query[] = "WHERE {$where}";

        self::execute(implode(" ", $query), $whereHolders);
        return (empty(self::$errors)) ? true : false;
    }

    /**
     * @param $table
     * @param $primary
     * @return bool|mixed
     */
    public static function emptyRow($table, $primary)
    {
        if (DB::insert($table, [$primary => 0])) {
            return SDbBase::getLastInsertedId();
        } else {
            return false;
        }
    }


    /**
     *
     */
    public static function transactionStart()
    {
        if (!isset(self::$connections[self::$current])) {
            self::connect();
        }

        try {
            self::$connections[self::$current]->beginTransaction();
        } catch (Exception $e) {
            self::$errors[] = $e->getMessage();
        }
    }

    /**
     *
     */
    public static function transactionRollBack()
    {
        if (!isset(self::$connections[self::$current])) {
            self::connect();
        }

        try {
            self::$connections[self::$current]->rollBack();
        } catch (Exception $e) {
            self::$errors[] = $e->getMessage();
        }
    }

    /**
     *
     */
    public static function transactionCommit()
    {
        if (!isset(self::$connections[self::$current])) {
            self::connect();
        }

        try {
            self::$connections[self::$current]->commit();
        } catch (Exception $e) {
            self::$errors[] = $e->getMessage();
        }
    }


}