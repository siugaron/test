<?php
namespace App;
class Debug
{

    const LOG_LEVEL_INFO = 0;
    const LOG_LEVEL_WARNING = 1;
    const LOG_LEVEL_CRITICAL = 2;
    const LOG_LEVEL_ERROR = 3;

    public static $log = array();
    public static $logDisable = false;

	/**
	 * @param $var
	 */
    public static function dump($var)
    {
		echo '<pre>';
		var_dump($var);
		echo "</pre>";
    }

    /**
     * @param Exception $e
     * @param int $level
     * @param null $module
     * @param array $options
     * @param bool $callback
     *
     * @return bool
     */
    public static function logException(Exception $e, $level = self::LOG_LEVEL_INFO, $module = null, array $options = array(), $callback = false)
    {
        self::logMsg($e->getMessage() . "\n" . $e->getTraceAsString() . "\n on " . $e->getLine(), "exceptions.log");
        if (is_array($callback) && $callback) {
            $class = (string)$callback[0];
            $callable = "{$class}::{$callback[1]}";
            if (@is_callable($callable) === true) {
                call_user_func_array($callable, $callback[2]);
            }
        }
        return true;
    }

    /**
     *
     * @param string $msg
     * @param string $file
     */
    public static function logMsg($msg, $file = 'common.log')
    {
        $path = PATH_REAL . "/logs/" . $file;
        if (strpos(DIRECTORY_SEPARATOR, $file) !== false) {
            $subfolders = explode(DIRECTORY_SEPARATOR, $file);
            array_pop($subfolders);
            $subdir = "";
            foreach ($subfolders as $i => $dir) {
                $subdir .= $dir . "/";
                if (!is_dir(PATH_REAL . "/logs/" . $subdir)) {
                    mkdir(PATH_REAL .  "/logs/" . $subdir);
                }
            }
        }
        $trace = debug_backtrace();
        $lastTrace = end($trace);
        if (is_file($path) && filesize($path) > 50 * 1024 * 1024) {
            $flag = "";
        } else {
            $flag = FILE_APPEND;
        }
        $lastTrace['file'] = isset($lastTrace['file']) ? $lastTrace['file'] : $lastTrace['class'];
        $lastTrace['line'] = isset($lastTrace['line']) ? $lastTrace['line'] : "not-set-line";

        $msg = (is_array($msg)) ? print_r($msg, true) : $msg;
        $msg = date("Y-m-d H:i:s") . " " . date_default_timezone_get() . "\n" . $lastTrace['file'] . ": " . $lastTrace['line'] . "\n" . $msg . " \n\r";
        if ($flag) {
            @file_put_contents($path, $msg, $flag);
        } else {
            file_put_contents($path, $msg);
        }
    }

    /**
     * @return array
     */
    public static function backtrace()
    {
        $d = debug_backtrace();
        $back = [];
        foreach ($d as $row) {
            if (isset($row['file']) && isset($row['line'])) {
                $back[] = $row['file'] . " - " . $row['line'] . " - " . $row['function'];
            }
        }
        return $back;
    }


    public static function consoleLog($data) {
        echo '<script>';
        echo 'console.log(' . json_encode($data) . ')';
        echo '</script>';
    }
}
