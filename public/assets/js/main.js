$(document).ready(function () {
    $('#add_task').on('submit', function (e) {
        e.preventDefault();
        let url = $(this).attr('action');

        $.ajax(url,{
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (data) {
                let responseBlock = $('#addtask .modal-response');
                if (data.error !== undefined) {
                    showError(responseBlock,data.error)
                }
                if (data.success !== undefined) {
                    showSuccess(responseBlock,data.success)
                }

            }
        })
    });
    $('#login_form').on('submit', function (e) {
        e.preventDefault();
        let url = $(this).attr('action');

        $.ajax(url,{
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (data) {
                let responseBlock = $('#login_modal .modal-response');
                if (data.error !== undefined) {
                    showError(responseBlock,data.error)
                }
                if (data.success !== undefined) {
                    showSuccess(responseBlock,data.success)
                }

            }
        })
    });
    $('.edit-task').on('click', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let target = $(this).attr('href');
        let text = $(this).parent().parent().find('.text').text();
        $('#edit_modal #id_task').val(id);
        $('#edit_modal #edit_form').attr('action',target);
        $('#edit_modal #editedtext').val(text);
        let myModal = new bootstrap.Modal(document.getElementById('edit_modal'));
        myModal.show();
    });
    $('#edit_form').on('submit', function (e) {
        e.preventDefault();
        let url = $(this).attr('action');

        $.ajax(url,{
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (data) {
                let responseBlock = $('#edit_modal .modal-response');
                if (data.error !== undefined) {
                    showError(responseBlock,data.error)
                }
                if (data.success !== undefined) {
                    showSuccess(responseBlock,data.success,0)
                }

            }
        })
    });


    $('#table_tasks').DataTable( {
        "lengthChange": false,
        "pagingType": "numbers",
        'pageLength': 3,
        "searching": false,
        "info":     false
    });
});

function showError(block, errors) {
    let htmlError = '';
    errors.forEach(function (currentValue, index, array) {
        htmlError += '<div class="alert alert-danger" role="alert">' +
            currentValue +
            '</div>';
    });
    block.html(htmlError)
}

function showSuccess(block, mes, timeout = 1000) {
    block.html('<div class="alert alert-success" role="alert">' +
        mes +
        '</div>')
    setTimeout(function(){
        window.location.reload(1);
    }, timeout);
}