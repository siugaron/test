<?php


namespace App;


class Redirect {
	public static function run($url = '/', $params = []) {
		header("Status: 301 Moved Permanently");
		header("Location:".PATH_SITE.$url."?". implode("&",\Loader::paramToString($params)));
	}
}