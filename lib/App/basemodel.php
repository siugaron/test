<?php


namespace App;


use App\DB\DB;

/**
 * Class BaseModel
 * @package App
 */
abstract class BaseModel {
	public $id;
	public $fields=[];
	public $table;

	/**
	 * BaseModel constructor.
	 *
	 * @param bool $id
	 */
	public function __construct($id = false) {
		if ($id) {
			$arr = DB::select($this->table,'*','id=?',[$id]);
			if ($arr && !empty($arr)) {
				$this->id = $id;
				$this->fields = $arr[0];
			}
		}
		return $this;
	}

	/**
	 * @param bool $order
	 * @param bool $page
	 * @param int $count
	 *
	 * @return mixed
	 */
	public function getAllByPage($order='id DESC', $page = false, $count = 3) {
		$limit = false;
		if ($page) {
			$limit = ($page-1)*$count.", ".$count;
		}

		$arr = DB::select($this->table,'*','',[],$order,$limit);
		return $arr;
	}

	public function count() {
		return DB::count($this->table);
	}

	/**
	 * @param $fields
	 *
	 * @return $this
	 */
	public function setFields( $fields ) {
		$this->fields = $fields;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function save() {

		if (!$this->id) {
			return DB::insert($this->table,$this->fields);
		} else {
			return DB::update($this->table,$this->fields,'id=?',[$this->id]);
		}
	}

	/**
	 * @param $name
	 * @param $value
	 *
	 * @return $this
	 */
	public function setField($name,$value) {
		$this->fields[$name] = $value;
		return $this;
	}
}