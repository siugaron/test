<?php


namespace App;


abstract class BaseController {
	public $templatePage;

	/**
	 * @return mixed
	 */
	public function getTemplatePage() {
		return $this->templatePage;
	}

	/**
	 * @param mixed $templatePage
	 */
	public function setTemplatePage( $templatePage ) {
		$this->templatePage = $templatePage;
	}
}