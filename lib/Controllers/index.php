<?php
namespace Controllers;

use App\BaseController;

use Models\Tasks;

class Index extends BaseController {
	public $templatePage = 'index.html';

	public function IndexAction($params = []) {
		$tasks = new Tasks();

		$order = 'id DESC';



		$return['tasks'] = $tasks->getAllByPage($order);
		if (!empty($return['tasks'])) {
			foreach ($return['tasks'] as &$task) {
				$task['statusText'] = Tasks::$status[$task['status']];
				$task['approve'] = '/task/approve/?id='.$task['id'];
				$task['edit'] = '/task/edit/';
			}
		}
		$return['isAdmin'] = (isset($_SESSION['isAdmin']))? $_SESSION['isAdmin'] : false;

		if ($return['isAdmin']) {
			$return['logout']['link'] = PATH_SITE.'/login/out/';
		}



		$return['loginform']['action'] = PATH_SITE.'/login/';

		$return['addform']['action'] = PATH_SITE.'/task/add/';
		return $return;

	}
}