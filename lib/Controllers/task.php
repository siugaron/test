<?php


namespace Controllers;


use App\BaseController;
use App\DB\DB;
use App\Redirect;
use Models\Tasks;

class Task extends BaseController {

	public function AddAction( $params = [] ) {
		$return = [];
		if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {
			$task  = new Tasks();
			$email = htmlspecialchars( $_POST["email"] );
			$name  = htmlspecialchars( $_POST['name'] );
			if ( empty( $name ) ) {
				$return['error'][] = 'Не верно указано имя';
			}
			if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
				$return['error'][] = 'Не верно указан E-mail';
			}
			if ( empty( $return['error'] ) ) {
				if ( $task->setFields( [
					"name"   => $name,
					'text'   => htmlspecialchars( $_POST['text'] ),
					'email'  => $email,
					'status' => 1
				] )->save() ) {
					$return['success'] = 'Данные успешно сохранены';
				} else {
					$return['error'][] = DB::getLastError();
				}
			}
		}
		echo json_encode( $return );
		die();
	}

	public function ApproveAction( $params = [] ) {
		if ( isset( $_SESSION['isAdmin'] ) && $_SESSION['isAdmin'] ) {
			if ( key_exists( 'id', $params ) ) {
				$id   = $params['id'];
				$task = new Tasks( $id );
				$task->setField( 'status', 2 )->save();
			}
		}
		Redirect::run();
	}

	public function EditAction( $params = [] ) {
		$return = [];
		if ( isset( $_SESSION['isAdmin'] ) && $_SESSION['isAdmin'] ) {

			if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {
				$id   = htmlspecialchars( $_POST["id_task"] );
				$task = new Tasks( $id );

				$text = htmlspecialchars( $_POST['editedtext'] );
				if ( $text != $task->fields['text'] ) {
					if ( $task->setField( 'text', $text )->setField( 'modified', 1 )->save() ) {
						$return['success'] = 'Данные успешно сохранены';
					} else {
						$return['error'][] = DB::getLastError();
					}
				} else {
					$return['success'] = 'Данные успешно сохранены';
				}
			}

		} else {
			$return['error'][] = "Вы не авторизованы";
		}
		echo json_encode( $return );
		die();
	}

}