<?php


namespace Controllers;


use App\BaseController;
use App\DB\DB;
use App\Redirect;


class Login extends BaseController {
	private $admin = 'admin';
	private $pass = '123';


	public function IndexAction($params = []) {
		$return = [];
		if ($_SERVER["REQUEST_METHOD"] == "POST") {


			$name  = htmlspecialchars( $_POST['login'] );
			$pass = htmlspecialchars( $_POST['pass'] );
			if ( empty( $name ) || $name !== $this->admin) {
				$return['error'][] = 'Не верно указано имя';
			}
			if ( empty($pass) || $pass !== $this->pass) {
				$return['error'][] = 'Не указан пароль';
			}
			if (empty($return['error'])) {
				$_SESSION['isAdmin'] = true;
				$return['success'] = 'Вы успешно авторизовались';
			}
		}
		echo json_encode($return);
		die();
	}

	public function OutAction($params=[]) {
		if ($_SESSION['isAdmin'] == true) {
			$_SESSION['isAdmin'] = false;
		}

		Redirect::run('/',$params);
	}
}