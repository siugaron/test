<?php
session_start();
define( "PATH_REAL", substr( dirname( __FILE__ ), 0, strlen( dirname( __FILE__ ) ) - 4 ) );
define( 'PATH_SITE', '' );
require_once( '../vendor/autoload.php' );


class Loader {

	public static $dbName = 'u003_2';
	public static $dbUser = 'u003_2';
	public static $dbHost = 'localhost';
	public static $dbPassword = '8V4w1H2x';

	/**
	 * @param $className
	 *
	 * @return bool
	 */
	public static function load( $className ) {
		$classPath = PATH_REAL . '/lib/' . strtolower( $className ) . '.php';

		if ( strpos( $className, '\\' ) !== false ) {
			$classArray    = explode( '\\', $className );
			$realClassName = array_pop( $classArray );
			$classPath     = PATH_REAL . '/lib/';
			foreach ( $classArray as $folder ) {
				$classPath .= $folder . '/';
			}
			$classPath .= strtolower( $realClassName ) . '.php';
		}

		if ( file_exists( $classPath ) ) {
			require $classPath;

			return true;
		} else {
			return false;
		}

	}

	public static function route() {
		$input = urldecode( $_SERVER['REQUEST_URI'] );
		$input = ( PATH_SITE != "" ) ? substr( $input, strlen( PATH_SITE ) + 2, strlen( $input ) )
			: substr( $input, 1, strlen( $input ) );


		if ( PATH_SITE != "" ) {
			$input = str_replace( "/" . PATH_SITE . "/", "", $input );
		}


		$parseUrl = parse_url( $input );

		$endPath = array();
		if ( isset( $parseUrl["query"] ) ) {
			parse_str( $parseUrl["query"], $param );
		} else {
			$param = [];
		}

		if ( isset( $parseUrl["path"] ) && $parseUrl["path"] != "/" ) {
			$endPath = trim( $parseUrl["path"], "/" );
		}

		$path = ( $endPath ) ? explode( "/", $endPath ) : array();

		$controller = ( isset( $path[0] ) && ! empty( $path[0] ) ? $path[0] : "index" );
		$action     = ( isset( $path[1] ) && ! empty( $path[1] ) ? $path[1] : "index" );
		$controller = '\\Controllers\\' . ucfirst( strtolower( $controller ) );
		$action     = ucfirst( strtolower( $action ) ) . 'Action';

		$page       = '404.html';
		$pageParams = [];

		if ( class_exists( $controller ) ) {
			$controllerObj = new $controller();
			if ( method_exists( $controller, $action ) ) {
				$pageParams = $controllerObj->$action( $param );
				$page       = $controllerObj->getTemplatePage();
			}
		}
		if ( ! is_array( $pageParams ) ) {
			$pageParams = [];
		}

		$loader = new \Twig\Loader\FilesystemLoader( PATH_REAL . '/templates/' );
		$twig   = new \Twig\Environment( $loader, [] );
		try {
			echo $twig->render( $page, $pageParams );
		} catch ( Exception $exception ) {
			echo $twig->render( '500.html', [ 'error' => $exception->getMessage() ] );
		}

	}

	public static function paramToString( $params, $include = false, $exclude = [] ) {
		$paramString = '';
		if ( is_array( $params ) ) {
			$tmp = [];
			foreach ( $params as $param => $value ) {
				if ( in_array( $param, $exclude ) ) {
					continue;
				}
				if ( is_array( $include ) ) {
					if ( key_exists( $param, $include ) ) {
						continue;
					}
				}
				$tmp[] = $param . '=' . $value;
			}
			if ( is_array( $include ) ) {
				foreach ( $include as $param => $value ) {
					$tmp[] = $param . '=' . $value;
				}
			}

			$paramString = implode( '&', $tmp );
			unset( $tmp );
		}

		return $paramString;
	}
}

try {
	spl_autoload_register( array( 'Loader', 'load' ) );
} catch ( LogicException $e ) {
	header( "HTTP/1.0 500 Internal Server Error" );
	echo $e->getMessage();
	exit();
}