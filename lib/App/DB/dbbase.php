<?php
namespace App\DB;
use App\Debug;
use Loader;
use PDO;

class DBBase
{

    public static $connections;
    public static $current = "db";
    public static $errors = [];

    const SQL_QUOTE = "`";

    /**
     * @param $key
     */
    public static function SetDb($key)
    {
        self::$current = $key;
    }


    /**
     * @return bool|string
     */
    public static function getLastError()
    {
        return (!empty(self::$errors))
            ? implode('<br>', self::$errors)
            : false;
    }

    /**
     * @return mixed
     */
    public static function connect()
    {
        if (!isset(self::$connections[self::$current])) {

            if (!isset(Loader::$dbName,Loader::$dbHost,Loader::$dbPassword,Loader::$dbUser))
                die ('Configuration not found');

            try {

                self::$connections[self::$current] = new \PDO(
                    'mysql:dbname=' . Loader::$dbName . ";" .
                    'host=' . Loader::$dbHost . ";charset=utf8;",
	                Loader::$dbUser,
	                Loader::$dbPassword
                );

                $stmt = self::$connections[self::$current]->prepare("SET sql_mode=?");
                $stmt->execute([implode(',', [
                    'STRICT_TRANS_TABLES',
                    'NO_ZERO_IN_DATE',
                    'NO_ZERO_DATE',
                    'ERROR_FOR_DIVISION_BY_ZERO',
                    'NO_AUTO_CREATE_USER',
                    'NO_ENGINE_SUBSTITUTION'
                ])]);

                self::execute('SET NAMES utf8;');

                self::$connections[self::$current]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$connections[self::$current]->setAttribute(PDO::ATTR_PERSISTENT, true);


            } catch (PDOException $e) {
                die ('DB Error');
            }

        }

    }

    /**
     * @param $query
     * @param array $arguments
     */
    public static function query($query, $arguments = [])
    {
        if (!is_array($arguments)) {
            $arguments = func_get_args();
            array_shift($arguments);
        }

        self::execute($query, $arguments);
    }


    /**
     * @param $query
     * @param array $holders
     *
     * @return mixed
     */
    public static function execute($query, $holders = [])
    {
        if (!isset(self::$connections[self::$current])) {
            self::connect();
        }


        $query = str_replace("%%", "%", $query);
        $stmt = self::$connections[self::$current]->prepare($query);


        try {
            if (!empty($holders)) {
 
                foreach ($holders as $key => $value) {

                    if (is_int($value))
                        $param = PDO::PARAM_INT;
                    elseif (is_float($value))
                        $param = PDO::PARAM_STR;
                    elseif (is_bool($value))
                        $param = PDO::PARAM_BOOL;
                    elseif (is_null($value))
                        $param = PDO::PARAM_NULL;
                    elseif (is_string($value))
                        $param = PDO::PARAM_STR;
                    else
                        $param = FALSE;

                    $paramNumber = $key + 1;

                    if ($param){
                        $stmt->bindValue($paramNumber, $value, $param);
                    }

                }
            }

            $stmt->execute();

        } catch (Exception $e) {

            Debug::logMsg(
                PHP_EOL . $e->getMessage() .
                PHP_EOL . $query .
                PHP_EOL . print_r($holders, true) .
                PHP_EOL . PHP_EOL,
                "db-errors-" . date("Y-m-d") . ".log"
            );

            self::$errors[] = $e->getMessage();
        }

        return $stmt;
    }

    /**
     * @return mixed
     */
    public static function getLastInsertedId()
    {
        return self::$connections[self::$current]->lastInsertId();
    }


}