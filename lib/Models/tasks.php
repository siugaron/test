<?php
namespace Models;

use App\BaseModel;

class Tasks extends BaseModel {
	public $table = 'tasks';

	public static $status = [
		'1' => "Новый",
		'2' => 'Выполнен',
	];

}